import React from 'react';

import Tooltip from '../src/index';
import Wrapper from './Wrapper';

import './arrowContent.css';

export default {
  component: Tooltip,
  title: 'Arrow Size And Distance',
};

export const ArrowSizeAndDistance = (args) => (
  <Wrapper extraStyles={{ display: 'flex', justifyContent: 'space-between' }}>
    <Tooltip content="This has an arrowSize of 20" className="target" tipContentClassName="" arrowSize={20} {...args}>
      Larger arrowSize
    </Tooltip>
    <Tooltip direction="down" content="This has an arrowSize of 5" className="target" tipContentClassName="" arrowSize={5} {...args}>
      Smaller arrowSize
    </Tooltip>
    <Tooltip content="This has a distance prop of 20" className="target" tipContentClassName="" distance={20} {...args}>
      Increase distance
    </Tooltip>
    <Tooltip direction="down" content="This has a distance prop of 0" className="target" tipContentClassName="" distance={0} {...args}>
      Decrease distance
    </Tooltip>
  </Wrapper>
);

ArrowSizeAndDistance.args = {
  isOpen: true,
};
