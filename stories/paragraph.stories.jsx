import React from 'react';

import Tooltip from '../src/index';
import Wrapper from './Wrapper';

export default {
  component: Tooltip,
  title: 'In A Paragraph',
};

export const InAParagraph = (args) => (
  <Wrapper>
    For&nbsp;
    <Tooltip content="Go to google" direction="right" tagName="span" {...args}>
      <a href="http://google.com" target="_blank" rel="noopener noreferrer">
        inline text
      </a>
    </Tooltip>
    , a right or left tip works nicely. The tip will try to go the desired way and flip if there is not enough&nbsp;
    <Tooltip content="Go to google" direction="right" tagName="span" {...args}>
      <a href="http://google.com" target="_blank" rel="noopener noreferrer">
        space
      </a>
    </Tooltip>
    .
    <p style={{ marginTop: 50, marginRight: 100, textAlign: 'right' }}>
      You can also force the direction of the tip and it will allow itself&nbsp;
      <Tooltip className="target" tipContentClassName="" content="this direction is forced" direction="right" tagName="span" forceDirection {...args}>
        to go off screen
      </Tooltip>
      .
    </p>
  </Wrapper>
);

InAParagraph.args = {
  isOpen: true,
};
