import React from 'react';

import Tooltip from '../src/index';
import Wrapper from './Wrapper';

export default {
  component: Tooltip,
  title: 'Colors',
};

export const Colors = (args) => (
  <Wrapper>
    You can pass&nbsp;
    <Tooltip
      tagName="span"
      className="target"
      tipContentClassName=""
      color="blue"
      background="red"
      content="The color for this is defined by props"
      {...args}
    >
      color options as props
    </Tooltip>
    &nbsp;or use a&nbsp;
    <Tooltip
      tagName="span"
      className="target"
      tipContentClassName="customTip"
      direction="right"
      content="The color for this tip is defined by examples/index.css"
      {...args}
    >
      css stylesheet.
    </Tooltip>
    <h3>Default Styles</h3>
    <p>pass the &quot;defaultStyles&quot; prop as true to get up and running quick and easy</p>
    <p>
      <Tooltip content="styled with defaults" className="target" tipContentClassName="" useDefaultStyles tagName="span" {...args}>
        See default styles
      </Tooltip>
    </p>
  </Wrapper>
);

Colors.args = {
  isOpen: true,
};
