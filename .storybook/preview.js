import { INITIAL_VIEWPORTS } from '@storybook/addon-viewport';

const newViewports = {
  storyshots: {
    name: 'storyshots',
    styles: {
      width: '800px',
      height: '600px',
    },
  },
  responsive: {
    name: 'Responsive',
    styles: {
      width: '100%',
      height: '100%',
    },
    type: 'desktop',
  },
};

export default {
  parameters: {
    viewport: {
      defaultViewport: 'storyshots',
      viewports: {
        ...INITIAL_VIEWPORTS,
        ...newViewports,
      },
    },
  },
};
