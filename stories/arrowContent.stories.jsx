import React from 'react';

import Tooltip from '../src/index';
import Wrapper from './Wrapper';

import './arrowContent.css';

export default {
  component: Tooltip,
  title: 'Custom Arrow Content',
};

export const CustomArrowContent = (args) => {
  const svgArrow = (
    <svg style={{ display: 'block' }} viewBox="0 0 21 11" width="20px" height="10px">
      <path
        d="M0,11 L9.43630703,1.0733987 L9.43630703,1.0733987 C10.1266203,0.3284971 11.2459708,0 11.936284,1.0733987 L20,11"
        style={{ stroke: 'gray', fill: 'white' }}
      />
    </svg>
  );

  return (
    <Wrapper flex>
      <Tooltip
        content="By default the text is above the element"
        className="target"
        background="white"
        color="black"
        tipContentClassName="border-tooltip"
        spacing={0}
        arrowContent={svgArrow}
        {...args}
      >
        Hover
      </Tooltip>
      <Tooltip
        content="It'll center if it has room"
        className="target"
        background="white"
        color="black"
        tipContentClassName="border-tooltip"
        spacing={0}
        arrowContent={svgArrow}
        {...args}
      >
        Centered
      </Tooltip>
      <Tooltip
        content="down"
        direction="down"
        className="target"
        background="white"
        color="black"
        tipContentClassName="border-tooltip"
        spacing={0}
        arrowContent={svgArrow}
        {...args}
      >
        Target is here
      </Tooltip>
      <Tooltip
        content="right"
        direction="right"
        className="target"
        background="white"
        color="black"
        tipContentClassName="border-tooltip"
        spacing={0}
        arrowContent={svgArrow}
        {...args}
      >
        Target is here
      </Tooltip>
      <Tooltip
        content="left"
        direction="left"
        className="target"
        background="white"
        color="black"
        tipContentClassName="border-tooltip"
        spacing={0}
        arrowContent={svgArrow}
        {...args}
      >
        Target is here
      </Tooltip>
    </Wrapper>
  );
};

CustomArrowContent.args = {
  isOpen: true,
};
