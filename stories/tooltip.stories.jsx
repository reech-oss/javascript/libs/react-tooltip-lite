import React from 'react';

import TooltipComponent from '../src/index';

import './stories.css';

export default {
  component: TooltipComponent,
  title: 'Tooltip',
  decorators: [
    (Story) => (
      <div style={{ width: 'max-content' }}>
        <Story />
      </div>
    ),
  ],
};

export const Tooltip = (args) => <TooltipComponent {...args}>Lorem ipsum dolor</TooltipComponent>;

Tooltip.args = {
  content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
};
