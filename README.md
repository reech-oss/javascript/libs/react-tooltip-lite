# React tooltip-lite

A lightweight and responsive tooltip. Forked from [react-tooltip-lite](https://github.com/bsidelinger912/react-tooltip-lite) to be up-to-date. Feel free to Post an [issue](https://gitlab.com/reech-oss/javascript/libs/react-tooltip-lite/-/issues) if you're looking to support more use cases.

## Getting started

### 1. Install with NPM

```shell
npm install react-tooltip-lite
```

### 2. Import into your react Component

```tsx
import Tooltip from "react-tooltip-lite";
```

### 3. Wrap any element with the Tooltip component to make it a target

```tsx
<Tooltip content="Go to google">
  <a href="http://google.com"> edge</a>
</Tooltip>
```

**CodePen demo**: [http://codepen.io/bsidelinger912/pen/WOdPNK](http://codepen.io/bsidelinger912/pen/WOdPNK)

## Styling

By default you need to style react-tooltip-lite with CSS, this allows for pseudo elements and some cool border tricks, as well as using css/sass/less variables and such to keep your colors consistent. (Note: as of version 1.2.0 you can also pass the "useDefaultStyles" prop which will allow you to use react-tooltip-lite without a stylesheet.)

Since the tooltip's arrow is created using the css border rule (<https://css-tricks.com/snippets/css/css-triangle/>), you'll want to specify the border-color for the arrow to set it's color.

### Here's an example stylesheet

```css
.react-tooltip-lite {
  background: #333;
  color: white;
}

.react-tooltip-lite-arrow {
  border-color: #333;
}
```

For more examples, see the **CodePen demo**: [http://codepen.io/bsidelinger912/pen/WOdPNK](http://codepen.io/bsidelinger912/pen/WOdPNK).

## Props

You can pass in props to define tip direction, styling, etc. Content is the only required prop.

| Name                | Type                | Description                                                                                                                                                                                                                                                                                                                 |
| ------------------- | ------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| content             | node (text or html) | the contents of your hover target                                                                                                                                                                                                                                                                                           |
| tagName             | string              | html tag used for className                                                                                                                                                                                                                                                                                                 |
| direction           | string              | the tip direction, defaults to up. Possible values are "up", "down", "left", "right" with optional modifier for alignment of "start" and "end". e.g. "left-start" will attempt tooltip on left and align it with the start of the target. If alignment modifier is not specified the default behavior is to align "middle". |
| forceDirection      | boolean             | Tells the tip to allow itself to render out of view if there's not room for the specified direction. If undefined or false, the tip will change direction as needed to render within the confines of the window.                                                                                                            |
| className           | string              | css class added to the rendered wrapper (and the tooltip if tooltipClassName is undefined) NOTE: in future versions className will only be applied to the wrapper element and not the tooltip                                                                                                                               |
| tipContentClassName | string              | css class added to the tooltip                                                                                                                                                                                                                                                                                              |
| tipContentHover     | boolean             |
| background          | string              | background color for the tooltip contents and arrow                                                                                                                                                                                                                                                                         |
| color               | string              | text color for the tooltip contents                                                                                                                                                                                                                                                                                         |
| padding             | string              | padding amount for the tooltip contents (defaults to '10px')                                                                                                                                                                                                                                                                |
| styles              | object              | style overrides for the target wrapper                                                                                                                                                                                                                                                                                      |
| eventOn             | string              | full name of supported react event to show the tooltip, e.g.: 'onClick'                                                                                                                                                                                                                                                     |
| eventOff            | string              | full name of supported react event to hide the tooltip, e.g.: 'onClick'                                                                                                                                                                                                                                                     |
| eventToggle         | string              | full name of supported react event to toggle the tooltip, e.g.: 'onClick', default hover toggling is disabled when using this option                                                                                                                                                                                        |
| useHover            | boolean             | whether to use hover to show/hide the tip, defaults to true                                                                                                                                                                                                                                                                 |
| useDefaultStyles    | boolean             | uses default colors for the tooltip, so you don't need to write any CSS for it                                                                                                                                                                                                                                              |
| isOpen              | boolean             | forces open/close state from a prop, overrides hover or click state                                                                                                                                                                                                                                                         |
| hoverDelay          | number              | the number of milliseconds to determine hover intent, defaults to 200                                                                                                                                                                                                                                                       |
| mouseOutDelay       | number              | the number of milliseconds to determine hover-end intent, defaults to the hoverDelay value                                                                                                                                                                                                                                  |
| arrow               | boolean             | Whether or not to have an arrow on the tooltip, defaults to true                                                                                                                                                                                                                                                            |
| arrowSize           | number              | Number in pixels of the size of the arrow, defaults to 10                                                                                                                                                                                                                                                                   |
| arrowContent        | node (text or html) | custom arrow contents, such as an SVG                                                                                                                                                                                                                                                                                       |
| distance            | number              | The distance from the tooltip to the target, defaults to 10px with an arrow and 3px without an arrow                                                                                                                                                                                                                        |
| zIndex              | number              | The zIndex of the tooltip, defaults to 1000                                                                                                                                                                                                                                                                                 |
| onToggle            | function            | if passed, this is called when the visibility of the tooltip changes.                                                                                                                                                                                                                                                       |

### Here's an example using more of the props

```tsx
<Tooltip
  content={
    <div>
      <h4 className="tip-heading">
        An unordered list to demo some html content
      </h4>
      <ul className="tip-list">
        <li>One</li>
        <li>Two</li>
        <li>Three</li>
        <li>Four</li>
        <li>Five</li>
      </ul>
    </div>
  }
  direction="right"
  tagName="span"
  className="target"
>
  Target content for big html tip
</Tooltip>
```

To see more usage examples, take look at the /example folder in the [source](https://gitlab.com/reech-oss/javascript/libs/react-tooltip-lite).
