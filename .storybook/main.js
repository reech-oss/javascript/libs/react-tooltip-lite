const path = require('path');

module.exports = {
  framework: {
    name: '@storybook/react-webpack5',
    options: {},
  },
  stories: ['../stories/**/*.stories.jsx'],
  addons: ['@storybook/addon-controls', '@storybook/addon-viewport'],
  webpackFinal: async (config, { configType }) => {
    // `configType` has a value of 'DEVELOPMENT' or 'PRODUCTION'
    // You can change the configuration based on that.
    // 'PRODUCTION' is used when building the static version of storybook.

    // Default Storybook CSS rule prevent us to load css files. We have to exclude it to set the css loader defined into our webpack config.
    const cssRule = config.module.rules.find((rule) => 'test.css'.match(rule.test));
    cssRule.exclude = [path.resolve(__dirname, '../')];

    config.module.rules.push({
      test: /\.css$/,
      use: ['style-loader', 'css-loader'],
    });

    return config;
  },
};
