import React from 'react';

import Tooltip from '../src/index';
import Wrapper from './Wrapper';

export default {
  component: Tooltip,
  title: 'Custom Events',
};

export const CustomEvents = (args) => (
  <Wrapper>
    <p>
      <Tooltip
        content="this uses hover but also closes on click"
        className="target"
        tipContentClassName=""
        tagName="span"
        eventOff="onClick"
        {...args}
      >
        Close on click
      </Tooltip>
    </p>

    <p>
      <Tooltip
        content="opens on a click and closes on mouse out"
        className="target"
        tipContentClassName=""
        tagName="span"
        eventOn="onClick"
        eventOff="onMouseOut"
        useHover={false}
        {...args}
      >
        Open on click
      </Tooltip>
    </p>

    <p>
      <Tooltip
        content="this uses hover but also closes on click"
        className="target"
        tipContentClassName=""
        tagName="span"
        eventToggle="onClick"
        {...args}
      >
        Toggle on click
      </Tooltip>
    </p>

    <p>
      <Tooltip
        content="this has a different delay for mousein and mouseout"
        className="target"
        tipContentClassName=""
        tagName="span"
        hoverDelay={400}
        mouseOutDelay={800}
        {...args}
      >
        With mouseOutDelay
      </Tooltip>
    </p>
  </Wrapper>
);
