import React from 'react';

import Tooltip from '../src/index';
import Wrapper from './Wrapper';

export default {
  component: Tooltip,
  title: 'Nested Targets',
};

export const NestedTargets = (args) => (
  <Wrapper extraProps={{ display: 'flex', justifyContent: 'space-between' }}>
    <Tooltip content="Nested Elements" tagName="span" {...args}>
      <button type="button">
        some text&nbsp;
        <span>a span</span>
      </button>
    </Tooltip>
  </Wrapper>
);

NestedTargets.args = {
  isOpen: true,
};
