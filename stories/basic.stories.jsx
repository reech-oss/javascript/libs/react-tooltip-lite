import React from 'react';

import Tooltip from '../src/index';
import Wrapper from './Wrapper';

export default {
  component: Tooltip,
  title: 'Basic',
};

export const Basic = (args) => (
  <Wrapper flex>
    <Tooltip content="By default the text is above the element" className="target" tipContentClassName="" {...args}>
      Hover
    </Tooltip>
    <Tooltip content="It'll center if it has room" className="target" tipContentClassName="" {...args}>
      Centered
    </Tooltip>
    <Tooltip content="you can specify 'direction' (up, down, left, right) too" direction="down" className="target" tipContentClassName="" {...args}>
      Target is here
    </Tooltip>
  </Wrapper>
);

Basic.args = {
  isOpen: true,
};
