import React from 'react';

import Tooltip from '../src/index';
import Wrapper from './Wrapper';

export default {
  component: Tooltip,
  title: 'Wrap An Image',
};

export const WrapAnImage = (args) => (
  <Wrapper>
    <Tooltip content="you can wrap images of course too" styles={{ display: 'inline-block' }} direction="up" {...args}>
      <img style={{ width: 200, height: 200 }} src="https://picsum.photos/seed/ldlfkdvjnfdvzrofd/200/200" alt="Random" />
    </Tooltip>

    <Tooltip
      content="this image is absolute positioned"
      styles={{ display: 'inline-block', position: 'absolute', top: '0', right: 0 }}
      direction="right"
      className="image2"
      {...args}
    >
      <img style={{ width: 200, height: 200 }} src="https://picsum.photos/seed/mdsdpfovkjszndsq/200/200" alt="Random" />
    </Tooltip>
  </Wrapper>
);

WrapAnImage.args = {
  isOpen: true,
};
