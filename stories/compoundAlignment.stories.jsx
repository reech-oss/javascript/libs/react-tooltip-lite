import React from 'react';

import Tooltip from '../src/index';

import Wrapper from './Wrapper';

export default {
  component: Tooltip,
  title: 'Compound Alignment',
};

export const CompoundAlignment = (args) => (
  <>
    <Wrapper flex>
      <div className="stacked-examples">
        <Tooltip content="you can have compound alignments" direction="right-start" className="target" tipContentClassName="" arrow={false} {...args}>
          right-start
        </Tooltip>

        <Tooltip content="you can have compound alignments" direction="right-end" className="target" tipContentClassName="" arrow={false} {...args}>
          right-end
        </Tooltip>
      </div>

      <div className="stacked-examples">
        <Tooltip content="you can have compound alignments" direction="left-start" className="target" tipContentClassName="" arrow={false} {...args}>
          left-start
        </Tooltip>

        <Tooltip content="you can have compound alignments" direction="left-end" className="target" tipContentClassName="" arrow={false} {...args}>
          left-end
        </Tooltip>
      </div>
    </Wrapper>

    <Wrapper flex>
      <div className="stacked-examples">
        <Tooltip content="you can have compound alignments" direction="up-start" className="target" tipContentClassName="" arrow={false} {...args}>
          top-start
        </Tooltip>

        <Tooltip content="you can have compound alignments" direction="down-start" className="target" tipContentClassName="" arrow={false} {...args}>
          down-start
        </Tooltip>
      </div>
      <div className="stacked-examples">
        <Tooltip content="you can have compound alignments" direction="up-end" className="target" tipContentClassName="" arrow={false} {...args}>
          top-end
        </Tooltip>

        <Tooltip content="you can have compound alignments" direction="down-end" className="target" tipContentClassName="" arrow={false} {...args}>
          down-end
        </Tooltip>
      </div>
    </Wrapper>
  </>
);

CompoundAlignment.args = {
  isOpen: true,
};
